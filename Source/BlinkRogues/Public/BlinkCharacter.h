// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
//#include "CharacterMovementComponent.h"
#include "BlinkCharacter.generated.h"

class UArrowComponent;
class UCapsuleComponent;
class UBlinkMovementComponent;
class UStaticMeshComponent;

UCLASS()
class BLINKROGUES_API ABlinkCharacter : public ACharacter
{
	GENERATED_BODY()

private:
	/** The main skeletal mesh associated with this Character (optional sub-object). */
	UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* Mesh2;

	/** Movement component used for movement logic in various movement modes (walking, falling, etc), containing relevant settings and functions to control movement. */
	///UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	///UBlinkMovementComponent* CharacterMovement2;

	/** The CapsuleComponent being used for movement collision (by CharacterMovement). Always treated as being vertically aligned in simple collision check functions. */
	///UPROPERTY(Category = Character, VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	///UCapsuleComponent* CapsuleComponent2;


public:
	// Sets default values for this character's properties
	ABlinkCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	/** Returns Mesh subobject **/
	FORCEINLINE class UStaticMeshComponent* GetMesh() const { return Mesh2; }

	/** Name of the MeshComponent. Use this name if you want to prevent creation of the component (with ObjectInitializer.DoNotCreateDefaultSubobject). */
	static FName MeshComponentName2;

	/** Returns CharacterMovement subobject **/
	///FORCEINLINE class UBlinkMovementComponent* GetCharacterMovement() const { return CharacterMovement2; }

	/** Name of the CharacterMovement component. Use this name if you want to use a different class (with ObjectInitializer.SetDefaultSubobjectClass). */
	///static FName CharacterMovementComponentName2;

	//** Returns CapsuleComponent subobject **/
	//FORCEINLINE class UCapsuleComponent* GetCapsuleComponent() const { return CapsuleComponent2; }

	/** Name of the CapsuleComponent. */
	//static FName CapsuleComponentName2;


	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
