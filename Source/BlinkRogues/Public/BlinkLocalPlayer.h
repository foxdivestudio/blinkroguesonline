// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/LocalPlayer.h"
#include "BlinkLocalPlayer.generated.h"

/**
 * 
 */
UCLASS()
class BLINKROGUES_API UBlinkLocalPlayer : public ULocalPlayer
{
	GENERATED_BODY()

		FSceneView* CalcSceneView(class FSceneViewFamily* ViewFamily,
			FVector& OutViewLocation,
			FRotator& OutViewRotation,
			FViewport* Viewport,
			class FViewElementDrawer* ViewDrawer = NULL,
			EStereoscopicPass StereoPass = eSSP_FULL) override;

	/**
	* Calculate the view init settings for drawing from this view actor
	*
	* @param	OutInitOptions - output view struct. Not every field is initialized, some of them are only filled in by CalcSceneView
	* @param	Viewport - current client viewport
	* @param	ViewDrawer - optional drawing in the view
	* @param	StereoPass - whether we are drawing the full viewport, or a stereo left / right pass
	* @return	true if the view options were filled in. false in various fail conditions.
	*/
	bool CalcSceneViewInitOptions2(
		struct FSceneViewInitOptions& OutInitOptions,
		FViewport* Viewport,
		class FViewElementDrawer* ViewDrawer = NULL,
		EStereoscopicPass StereoPass = eSSP_FULL);

public:
	bool GetProjectionData(FViewport* Viewport, EStereoscopicPass StereoPass, FSceneViewProjectionData& ProjectionData) const;

	/** The coordinates for the upper left corner of the master viewport subregion allocated to this player. 0-1 */
	FVector2D Origin2;

	/** The size of the master viewport subregion allocated to this player. 0-1 */
	FVector2D Size2;

	virtual void PostInitProperties() override;
	virtual void FinishDestroy() override;
	static void AddReferencedObjects(UObject* InThis, FReferenceCollector& Collector);

private:
	FSceneViewStateReference BlinkViewState;
	FSceneViewStateReference BlinkViewState2;

};
