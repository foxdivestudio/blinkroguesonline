// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BlinkViewportClient.h"
#include "Camera/CameraActor.h"
#include "BlinkPlayerController.generated.h"

/**
 * 
 */
UCLASS(config = Game, BlueprintType, Blueprintable)
class BLINKROGUES_API ABlinkPlayerController : public APlayerController
{
	GENERATED_BODY()


public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ACameraActor* CameraActor2 = NULL;

	/**
	* Convert a World Space 3D position into a 2D Screen Space position.
	* @return true if the world coordinate was successfully projected to the screen.
	*/
	UFUNCTION(BlueprintCallable, Category = "Game|Player", meta = (DisplayName = "BlinkConvertWorldLocationToScreenLocation", Keywords = "project"))
	bool BlinkProjectWorldLocationToScreen(FVector WorldLocation, FVector2D& ScreenLocation, int ScreenIndex, bool bPlayerViewportRelative = false) const;

	UFUNCTION(BlueprintCallable, Category = "Viewport", meta = (DisplayName = "DisableSplitScreen", Keywords = "project"))
	void DisableSplitScreen(bool bDisable) const;

};