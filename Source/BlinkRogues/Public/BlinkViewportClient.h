// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameViewportClient.h"
#include "BlinkViewportClient.generated.h"

/**
 * 
 */
UCLASS(Within = Engine, transient, config = Engine)
class BLINKROGUES_API UBlinkViewportClient : public UGameViewportClient
{
	GENERATED_BODY()

private:
	bool bDisableSplitScreenOverride2; // wtf epic

									  /** Delegate called when the engine starts drawing a game viewport */
	FSimpleMulticastDelegate BeginDrawDelegate;

	/** Delegate called when the game viewport is drawn, before drawing the console */
	FSimpleMulticastDelegate DrawnDelegate;

	/** Delegate called when the engine finishes drawing a game viewport */
	FSimpleMulticastDelegate EndDrawDelegate;

	/** Current buffer visualization mode for this game viewport */
	FName CurrentBufferVisualizationMode;

	/** Whether or not this audio device is in audio-focus */
	bool bHasAudioFocus;


public:
	virtual void DisableSplitScreen(bool bDisable);

	virtual void UpdateActiveSplitscreenType() override;

	virtual void Draw(FViewport* Viewport, FCanvas* SceneCanvas) override;

	/** Called before rendering to allow the game viewport to allocate subregions to players. */
	virtual void LayoutPlayers() override;
};
