// Fill out your copyright notice in the Description page of Project Settings.

#include "BlinkCharacter.h"

#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "Engine/CollisionProfile.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

FName ABlinkCharacter::MeshComponentName2(TEXT("CharacterMesh2"));
///FName ABlinkCharacter::CharacterMovementComponentName2(TEXT("CharMoveComp2"));
//FName ABlinkCharacter::CapsuleComponentName2(TEXT("CollisionCylinder2"));

// Sets default values
ABlinkCharacter::ABlinkCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	
	// Character rotation only changes in Yaw, to prevent the capsule from changing orientation.
	// Ask the Controller for the full rotation if desired (ie for aiming).
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = true;

	/*CapsuleComponent2 = CreateDefaultSubobject<UCapsuleComponent>(ABlinkCharacter::CapsuleComponentName2);
	CapsuleComponent2->InitCapsuleSize(34.0f, 88.0f);
	CapsuleComponent2->SetCollisionProfileName(UCollisionProfile::Pawn_ProfileName);

	CapsuleComponent2->CanCharacterStepUpOn = ECB_No;
	CapsuleComponent2->SetShouldUpdatePhysicsVolume(true);
	CapsuleComponent2->bCheckAsyncSceneOnMove = false;
	CapsuleComponent2->SetCanEverAffectNavigation(false);
	CapsuleComponent2->bDynamicObstacle = true;
	RootComponent = CapsuleComponent2;*/

	bClientCheckEncroachmentOnNetUpdate = true;
	JumpKeyHoldTime = 0.0f;
	JumpMaxHoldTime = 0.0f;
	JumpMaxCount = 1;
	JumpCurrentCount = 0;
	bWasJumping = false;

	AnimRootMotionTranslationScale = 1.0f;



	///CharacterMovement2 = CreateDefaultSubobject<UBlinkMovementComponent>(ABlinkCharacter::CharacterMovementComponentName2);
	///if (CharacterMovement2)
	///{
	///	CharacterMovement2->UpdatedComponent = GetCapsuleComponent();
	///	CrouchedEyeHeight = CharacterMovement2->CrouchedHalfHeight * 0.80f;
	///}

	Mesh2 = CreateOptionalDefaultSubobject<UStaticMeshComponent>(ABlinkCharacter::MeshComponentName2);
	if (Mesh2)
	{
		Mesh2->AlwaysLoadOnClient = true;
		Mesh2->AlwaysLoadOnServer = true;
		Mesh2->bOwnerNoSee = false;
		
		Mesh2->bCastDynamicShadow = true;
		Mesh2->bAffectDynamicIndirectLighting = true;
		Mesh2->PrimaryComponentTick.TickGroup = TG_PrePhysics;
		Mesh2->SetupAttachment(GetCapsuleComponent());
		static FName MeshCollisionProfileName(TEXT("CharacterMesh"));
		Mesh2->SetCollisionProfileName(MeshCollisionProfileName);
		Mesh2->SetGenerateOverlapEvents(false);
		Mesh2->SetCanEverAffectNavigation(false);
	}

	BaseRotationOffset = FQuat::Identity;

	GetCharacterMovement()->SetMovementMode(MOVE_Flying);
	//GetCharacterMovement()->

}

// Called when the game starts or when spawned
void ABlinkCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlinkCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABlinkCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

