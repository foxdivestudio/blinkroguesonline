// Fill out your copyright notice in the Description page of Project Settings.

#include "BlinkPlayerController.h"

#include "BlinkLocalPlayer.h"

#include "BlinkRogues.h"



bool ABlinkPlayerController::BlinkProjectWorldLocationToScreen(FVector WorldLocation, FVector2D& ScreenLocation, int ScreenIndex, bool bPlayerViewportRelative) const
{
	UBlinkLocalPlayer* LP = (UBlinkLocalPlayer*)GetLocalPlayer();
	if (LP && LP->ViewportClient)
	{
		// get the projection data
		FSceneViewProjectionData ProjectionData;
		if (LP->GetProjectionData(LP->ViewportClient->Viewport, ScreenIndex == 0 ? eSSP_FULL : eSSP_LEFT_EYE, /*out*/ ProjectionData))
		{
			FMatrix const ViewProjectionMatrix = ProjectionData.ComputeViewProjectionMatrix();
			const bool bResult = FSceneView::ProjectWorldToScreen(WorldLocation, ProjectionData.GetConstrainedViewRect(), ViewProjectionMatrix, ScreenLocation);

			if (bPlayerViewportRelative)
			{
				ScreenLocation -= FVector2D(ProjectionData.GetViewRect().Min);
			}

			return bResult;
		}
	}

	ScreenLocation = FVector2D::ZeroVector;
	return false;
}


void ABlinkPlayerController::DisableSplitScreen(bool bDisable) const
{
	((UBlinkViewportClient*)GetLocalPlayer()->GetWorld()->GetGameViewport())->DisableSplitScreen(bDisable);
	GetLocalPlayer()->GetWorld()->GetGameViewport()->SetDisableSplitscreenOverride(bDisable);
}

