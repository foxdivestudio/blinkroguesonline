// Fill out your copyright notice in the Description page of Project Settings.

#include "BlinkLocalPlayer.h"

#include "BlinkRogues.h"
#include "SceneViewExtension.h"
#include "EngineStats.h"
#include "BlinkPlayerController.h"

bool UBlinkLocalPlayer::GetProjectionData(FViewport* Viewport, EStereoscopicPass StereoPass, FSceneViewProjectionData& ProjectionData) const
{
	// If the actor
	if ((Viewport == NULL) || (PlayerController == NULL) || (Viewport->GetSizeXY().X == 0) || (Viewport->GetSizeXY().Y == 0))
	{
		return false;
	}

	int32 X = FMath::TruncToInt((StereoPass == eSSP_FULL ? Origin.X : Origin2.X) * Viewport->GetSizeXY().X);
	int32 Y = FMath::TruncToInt((StereoPass == eSSP_FULL ? Origin.Y : Origin2.Y) * Viewport->GetSizeXY().Y);
	uint32 SizeX = FMath::TruncToInt((StereoPass == eSSP_FULL ? Size.X : Size2.X) * Viewport->GetSizeXY().X);
	uint32 SizeY = FMath::TruncToInt((StereoPass == eSSP_FULL ? Size.Y : Size2.Y) * Viewport->GetSizeXY().Y);

	FIntRect UnconstrainedRectangle = FIntRect(X, Y, X + SizeX, Y + SizeY);

	ProjectionData.SetViewRectangle(UnconstrainedRectangle);

	// Get the viewpoint.
	FMinimalViewInfo ViewInfo;
	GetViewPoint(/*out*/ ViewInfo, eSSP_FULL);
	if (PlayerController != NULL)
	{
		if (StereoPass != eSSP_FULL && ((ABlinkPlayerController*)PlayerController)->CameraActor2 != NULL)
		{
			ViewInfo.Location = ((ABlinkPlayerController*)PlayerController)->CameraActor2->GetActorLocation();
		}
	}
	// scale distances for cull distance purposes by the ratio of our current FOV to the default FOV
	PlayerController->LocalPlayerCachedLODDistanceFactor = ViewInfo.FOV / FMath::Max<float>(0.01f, (PlayerController->PlayerCameraManager != NULL) ? PlayerController->PlayerCameraManager->DefaultFOV : 90.f);

	// Create the view matrix

	ProjectionData.ViewOrigin = ViewInfo.Location;
	ProjectionData.ViewRotationMatrix = FInverseRotationMatrix(ViewInfo.Rotation) * FMatrix(
		FPlane(0, 0, 1, 0),
		FPlane(1, 0, 0, 0),
		FPlane(0, 1, 0, 0),
		FPlane(0, 0, 0, 1));


	// Create the projection matrix (and possibly constrain the view rectangle)
	FMinimalViewInfo::CalculateProjectionMatrixGivenView(ViewInfo, AspectRatioAxisConstraint, ViewportClient->Viewport, /*inout*/ ProjectionData);

	return true;
}


FSceneView* UBlinkLocalPlayer::CalcSceneView(class FSceneViewFamily* ViewFamily,
	FVector& OutViewLocation,
	FRotator& OutViewRotation,
	FViewport* Viewport,
	class FViewElementDrawer* ViewDrawer,
	EStereoscopicPass StereoPass)
{
	//SCOPE_CYCLE_COUNTER(STAT_CalcSceneView);

	FSceneViewInitOptions ViewInitOptions;

	if (!CalcSceneViewInitOptions2(ViewInitOptions, Viewport, ViewDrawer, StereoPass))
	{
		return nullptr;
	}

	// Get the viewpoint...technically doing this twice
	// but it makes GetProjectionData better
	FMinimalViewInfo ViewInfo;
	GetViewPoint(ViewInfo, eSSP_FULL);
	OutViewLocation = ViewInfo.Location;
	OutViewRotation = ViewInfo.Rotation;
	ViewInitOptions.bUseFieldOfViewForLOD = ViewInfo.bUseFieldOfViewForLOD;

	// Fill out the rest of the view init options
	ViewInitOptions.ViewFamily = ViewFamily;

	{
		QUICK_SCOPE_CYCLE_COUNTER(STAT_BuildHiddenComponentList);
		PlayerController->BuildHiddenComponentList(OutViewLocation, /*out*/ ViewInitOptions.HiddenPrimitives);
	}

	//@TODO: SPLITSCREEN: This call will have an issue with splitscreen, as the show flags are shared across the view family
	EngineShowFlagOrthographicOverride(ViewInitOptions.IsPerspectiveProjection(), ViewFamily->EngineShowFlags);

	FSceneView* const View = new FSceneView(ViewInitOptions);

	View->ViewLocation = OutViewLocation;
	View->ViewRotation = OutViewRotation;

	ViewFamily->Views.Add(View);

	{
		View->StartFinalPostprocessSettings(OutViewLocation);

		// CameraAnim override
		if (PlayerController->PlayerCameraManager)
		{
			TArray<FPostProcessSettings> const* CameraAnimPPSettings;
			TArray<float> const* CameraAnimPPBlendWeights;
			PlayerController->PlayerCameraManager->GetCachedPostProcessBlends(CameraAnimPPSettings, CameraAnimPPBlendWeights);

			for (int32 PPIdx = 0; PPIdx < CameraAnimPPBlendWeights->Num(); ++PPIdx)
			{
				View->OverridePostProcessSettings((*CameraAnimPPSettings)[PPIdx], (*CameraAnimPPBlendWeights)[PPIdx]);
			}
		}

		//	CAMERA OVERRIDE
		//	NOTE: Matinee works through this channel
		View->OverridePostProcessSettings(ViewInfo.PostProcessSettings, ViewInfo.PostProcessBlendWeight);

		View->EndFinalPostprocessSettings(ViewInitOptions);
	}

	for (int ViewExt = 0; ViewExt < ViewFamily->ViewExtensions.Num(); ViewExt++)
	{
		ViewFamily->ViewExtensions[ViewExt]->SetupView(*ViewFamily, *View);
	}

	return View;
}


bool UBlinkLocalPlayer::CalcSceneViewInitOptions2(
	struct FSceneViewInitOptions& ViewInitOptions,
	FViewport* Viewport,
	class FViewElementDrawer* ViewDrawer,
	EStereoscopicPass StereoPass)
{
	QUICK_SCOPE_CYCLE_COUNTER(STAT_CalcSceneViewInitOptions);
	if ((PlayerController == NULL) || (Size.X <= 0.f) || (Size.Y <= 0.f) || (Viewport == NULL))
	{
		return false;
	}
	// get the projection data
	if (GetProjectionData(Viewport, StereoPass, /*inout*/ ViewInitOptions) == false)
	{
		// Return NULL if this we didn't get back the info we needed
		return false;
	}

	// return if we have an invalid view rect
	if (!ViewInitOptions.IsValidViewRectangle())
	{
		return false;
	}

	if (PlayerController->PlayerCameraManager != NULL)
	{
		// Apply screen fade effect to screen.
		if (PlayerController->PlayerCameraManager->bEnableFading)
		{
			ViewInitOptions.OverlayColor = PlayerController->PlayerCameraManager->FadeColor;
			ViewInitOptions.OverlayColor.A = FMath::Clamp(PlayerController->PlayerCameraManager->FadeAmount, 0.0f, 1.0f);
		}

		// Do color scaling if desired.
		if (PlayerController->PlayerCameraManager->bEnableColorScaling)
		{
			ViewInitOptions.ColorScale = FLinearColor(
				PlayerController->PlayerCameraManager->ColorScale.X,
				PlayerController->PlayerCameraManager->ColorScale.Y,
				PlayerController->PlayerCameraManager->ColorScale.Z
			);
		}

		// Was there a camera cut this frame?
		ViewInitOptions.bInCameraCut = PlayerController->PlayerCameraManager->bGameCameraCutThisFrame;
	}

	check(PlayerController && PlayerController->GetWorld());
	ViewInitOptions.SceneViewStateInterface = (StereoPass == eSSP_FULL) ? BlinkViewState.GetReference() : BlinkViewState2.GetReference();
	ViewInitOptions.ViewActor = (StereoPass == eSSP_FULL) ? PlayerController->GetViewTarget() : ((ABlinkPlayerController*)PlayerController)->CameraActor2;
	ViewInitOptions.ViewElementDrawer = ViewDrawer;
	ViewInitOptions.BackgroundColor = FLinearColor::Black;
	ViewInitOptions.LODDistanceFactor = PlayerController->LocalPlayerCachedLODDistanceFactor;
	ViewInitOptions.StereoPass = eSSP_FULL;
	ViewInitOptions.WorldToMetersScale = PlayerController->GetWorldSettings()->WorldToMeters;
	ViewInitOptions.CursorPos = Viewport->HasMouseCapture() ? FIntPoint(-1, -1) : FIntPoint(Viewport->GetMouseX(), Viewport->GetMouseY());
	ViewInitOptions.OriginOffsetThisFrame = PlayerController->GetWorld()->OriginOffsetThisFrame;

	return true;
}

void UBlinkLocalPlayer::PostInitProperties()
{
	Super::PostInitProperties();
	if (!IsTemplate())
	{
		BlinkViewState.Allocate();
		BlinkViewState2.Allocate();
	}
}

void UBlinkLocalPlayer::FinishDestroy()
{
	if (!IsTemplate())
	{
		BlinkViewState.Destroy();
		BlinkViewState2.Destroy();
	}
	Super::FinishDestroy();
}

void UBlinkLocalPlayer::AddReferencedObjects(UObject* InThis, FReferenceCollector& Collector)
{
	UBlinkLocalPlayer* This = CastChecked<UBlinkLocalPlayer>(InThis);

	FSceneViewStateInterface* Ref = This->BlinkViewState.GetReference();
	if (Ref)
	{
		Ref->AddReferencedObjects(Collector);
	}

	FSceneViewStateInterface* Ref2 = This->BlinkViewState2.GetReference();
	if (Ref2)
	{
		Ref2->AddReferencedObjects(Collector);
	}

	ULocalPlayer::AddReferencedObjects(This, Collector);
}
